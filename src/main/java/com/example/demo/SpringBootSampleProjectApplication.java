package com.example.demo;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.xml.StaxUtils;

@SpringBootApplication
public class SpringBootSampleProjectApplication {

	public static void main(String[] args)
	{
		ConfigurableApplicationContext context = SpringApplication.run(SpringBootSampleProjectApplication.class, args);
		//System.out.println("First Application");

		/*Alien a = context.getBean(Alien.class);
		a.show();*/
	}

}
