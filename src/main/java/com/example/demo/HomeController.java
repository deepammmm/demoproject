package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class HomeController
{
    @RequestMapping("home")
    //This is used when we want to return data not the page
    //@ResponseBody
    /*public String home (HttpServletRequest request)
    {
        HttpSession session = request.getSession();
        System.out.println("controller is being called");
        String name  = request.getParameter("name");
        System.out.println("name is" +name);
        request.setAttribute("name",name);
        return "home";
    }*/

    //it is taking name from the request localhost:8080/home?name=gaurav and it should match also from the request parameter
    // We are processing data that is model and returning a view hence we have no need of session
   /* public String home (String name, HttpSession session)
    {
        System.out.println("controller is being called");
        session.setAttribute("name",name);
        return "home";
    }*/

   //implementation using model and view
    //Request param used for define the parameter name in the request , if there  are multiple parameters
   /* public ModelAndView home (@RequestParam("name") String myName)
    {
        ModelAndView mv = new ModelAndView();
        System.out.println("controller is being called");
        mv.addObject("name",myName);
        mv.setViewName("home");
        //session.setAttribute("name",myName);
        return mv;
    }*/

    // This is for passing the object directly
    public ModelAndView home (Alien alien)
    {
        ModelAndView mv = new ModelAndView();
        System.out.println("controller is being called");
        mv.addObject("obj",alien);
        mv.setViewName("home");
        //session.setAttribute("name",myName);
        return mv;
    }

}
